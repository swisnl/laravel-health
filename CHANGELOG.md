# Changelog

All notable changes to `swisnl/laravel-health` will be documented in this file.

## [Unreleased]

* Nothing.

## [1.6.1] - 2025-02-25

### Added

* Added support for Laravel 12.

## [1.6.0] - 2025-02-03

### Changed

* The schedule check is now enabled by default, as we can't reliably auto-discover it in Laravel 11. If your project doesn't have scheduled commands, you should disable this check manually with `HEALTH_CHECK_SCHEDULE_ENABLED=false`.

### Fixed

* Fixed an issue where the heartbeat command wasn't added to the schedule in Laravel 11 projects.

## [1.5.0] - 2024-11-15

### Added

* Added Elasticsearch health check.
* Added Mail health check.
* Added Sentry health check.

## [1.4.2] - 2024-11-14

### Changed

* Use base_path helper to get storage path.

## [1.4.1] - 2024-06-06

### Fixed

* Also set defaults in config to fix TypeError when config is not published in project.

## [1.4.0] - 2024-03-28

### Added

* Added support for Laravel 11.

## [1.3.1] - 2024-03-20

### Fixed

* Moved config defaults to code to fix TypeError when config is published in project.

## [1.3.0] - 2024-03-05

### Added

* Thresholds of `UsedDiskSpaceCheck` are now configurable.

## [1.2.1] - 2024-02-26

### Fixed

* Added custom `UsedDiskSpaceCheck` with workaround for bug in BusyBox `df` command (https://bugs.busybox.net/show_bug.cgi?id=15958).

## [1.2.0] - 2023-10-11

### Added

* Check validity of security.txt (if present).

## [1.1.0] - 2023-10-11

### Added

* Allow disabling of default checks.

## [1.0.1] - 2023-10-04

### Fixed

* Skip used disk space check if app doesn't have files.
