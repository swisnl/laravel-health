<?php

return [
    /*
     * You can configure which checks should be performed. There are three options:
     *  - null: let this package auto-discover if this check should be performed
     *  - true: force the check to be performed
     *  - false: disable the check
     */
    'checks' => [
        /*
         * @see https://spatie.be/docs/laravel-health/v1/available-checks/cache
         */
        'cache' => [
            'enabled' => env('HEALTH_CHECK_CACHE_ENABLED'),
        ],
        /*
         * @see https://spatie.be/docs/laravel-health/v1/available-checks/db-connection
         */
        'database' => [
            'enabled' => env('HEALTH_CHECK_DATABASE_ENABLED'),
        ],
        /*
         * @see https://spatie.be/docs/laravel-health/v1/available-checks/debug-mode
         */
        'debug_mode' => [
            'enabled' => env('HEALTH_CHECK_DEBUG_MODE_ENABLED'),
        ],
        /*
         * @see \Swis\Health\Checks\ElasticsearchCheck
         */
        'elasticsearch' => [
            'enabled' => env('HEALTH_CHECK_ELASTICSEARCH_ENABLED'),
        ],
        /*
         * @see https://spatie.be/docs/laravel-health/v1/available-checks/environment
         */
        'environment' => [
            'enabled' => env('HEALTH_CHECK_ENVIRONMENT_ENABLED'),
        ],
        /*
         * @see https://spatie.be/docs/laravel-health/v1/available-checks/horizon
         */
        'horizon' => [
            'enabled' => env('HEALTH_CHECK_HORIZON_ENABLED'),
        ],
        /*
         * @see \Swis\Health\Checks\MailCheck
         */
        'mail' => [
            'enabled' => env('HEALTH_CHECK_MAIL_ENABLED'),
        ],
        /*
         * @see https://spatie.be/docs/laravel-health/v1/available-checks/cached-config-routes-and-events
         */
        'optimized_app' => [
            'enabled' => env('HEALTH_CHECK_OPTIMIZED_APP_ENABLED'),
        ],
        /*
         * @see https://spatie.be/docs/laravel-health/v1/available-checks/queue
         */
        'queue' => [
            'enabled' => env('HEALTH_CHECK_QUEUE_ENABLED'),
        ],
        /*
         * @see https://spatie.be/docs/laravel-health/v1/available-checks/schedule
         */
        'schedule' => [
            'enabled' => env('HEALTH_CHECK_SCHEDULE_ENABLED'),
        ],
        /*
         * @see \Swis\Health\Checks\SecurityTxtCheck
         */
        'security_txt' => [
            'enabled' => env('HEALTH_CHECK_SECURITY_TXT_ENABLED'),
        ],
        /*
         * @see \Swis\Health\Checks\SentryCheck
         */
        'sentry' => [
            'enabled' => env('HEALTH_CHECK_SENTRY_ENABLED'),
        ],
        /*
         * @see https://spatie.be/docs/laravel-health/v1/available-checks/used-disk-space
         */
        'used_disk_space' => [
            'enabled' => env('HEALTH_CHECK_USED_DISK_SPACE_ENABLED'),
            'warning_threshold' => env('HEALTH_CHECK_USED_DISK_SPACE_WARNING_THRESHOLD', 85),
            'error_threshold' => env('HEALTH_CHECK_USED_DISK_SPACE_ERROR_THRESHOLD', 95),
        ],
    ],
];
