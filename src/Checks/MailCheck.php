<?php

declare(strict_types=1);

namespace Swis\Health\Checks;

use Illuminate\Support\Facades\Mail;
use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;
use Symfony\Component\Mailer\Exception\ExceptionInterface;

class MailCheck extends Check
{
    public function run(): Result
    {
        $result = Result::make();

        if (config('mail.default') !== 'smtp') {
            return $result->warning(sprintf('The mailer was expected to be `smtp`, but actually was `%s`', config('mail.default')));
        }

        try {
            Mail::getSymfonyTransport()->start();

            return $result->ok();
        } catch (ExceptionInterface $e) {
            return $result->failed($e->getMessage());
        }
    }
}
