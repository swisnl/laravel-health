<?php

namespace Swis\Health\Checks;

use Spatie\Health\Checks\Checks\UsedDiskSpaceCheck as BaseCheck;
use Spatie\Regex\Regex;
use Symfony\Component\Process\Process;

class UsedDiskSpaceCheck extends BaseCheck
{
    protected function getDiskUsagePercentage(): int
    {
        $process = Process::fromShellCommandline($this->filesystemName ? sprintf('df -P | grep %s$', $this->filesystemName) : 'df -P .');

        $process->run();

        $output = $process->getOutput();

        return (int) Regex::match('/(\d*)%/', $output)->group(1);
    }
}
