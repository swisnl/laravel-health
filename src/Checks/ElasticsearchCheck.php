<?php

declare(strict_types=1);

namespace Swis\Health\Checks;

use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Elastic\Elasticsearch\Exception\ServerResponseException;
use Elastic\Transport\Exception\NoNodeAvailableException;
use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;

class ElasticsearchCheck extends Check
{
    public function run(): Result
    {
        /** @var \Elastic\Elasticsearch\Client $client */
        $client = app(Client::class);

        $result = Result::make();

        try {
            $client->ping();

            return $result->ok();
        } catch (NoNodeAvailableException|ClientResponseException|ServerResponseException $e) {
            return $result->failed($e->getMessage());
        }
    }
}
