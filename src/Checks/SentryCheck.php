<?php

declare(strict_types=1);

namespace Swis\Health\Checks;

use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;

class SentryCheck extends Check
{
    public function run(): Result
    {
        $result = Result::make()
            ->meta(['release' => config('sentry.release'), 'environment' => config('sentry.environment') ?: config('app.env')]);

        if (! config('sentry.dsn')) {
            return $result->failed('Sentry DSN missing');
        }

        return $result->ok();
    }
}
