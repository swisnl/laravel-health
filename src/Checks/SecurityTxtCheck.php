<?php

declare(strict_types=1);

namespace Swis\Health\Checks;

use Carbon\Carbon;
use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;
use Swis\Health\Support\SecurityTxt;

class SecurityTxtCheck extends Check
{
    protected ?string $label = 'Security.txt';

    protected ?string $file = null;

    protected ?string $url = null;

    protected int $warningThreshold = 21;

    protected int $errorThreshold = 7;

    public function file(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function url(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function warnWhenSecurityTxtExpiringIn(int $days): self
    {
        $this->warningThreshold = $days;

        return $this;
    }

    public function failWhenSecurityTxtExpiringIn(int $days): self
    {
        $this->errorThreshold = $days;

        return $this;
    }

    public function run(): Result
    {
        if (is_null($this->file) && is_null($this->url)) {
            return Result::make()
                ->failed()
                ->shortSummary('When using the `SecurityTxtCheck` you must call `file` or `url` to pass the file or URL you want to check.');
        }

        if (! is_null($this->file) && ! is_null($this->url)) {
            return Result::make()
                ->failed()
                ->shortSummary('When using the `SecurityTxtCheck` you must call either `file` or `url` to pass the file or URL you want to check, but not both.');
        }

        $securityTxt = $this->file ? SecurityTxt::createFromFile($this->file) : SecurityTxt::createFromUrl($this->url);
        $daysUntilExpired = Carbon::now()->floatDiffInDays($securityTxt->expirationDate(), false);
        $daysUntilExpiredRounded = (int) round($daysUntilExpired);

        $result = Result::make()
            ->meta(['days_until_expired' => $daysUntilExpiredRounded])
            ->shortSummary($daysUntilExpiredRounded.' days until');

        if ($daysUntilExpired < 0) {
            return $result->failed('Security.txt has expired');
        }

        if ($daysUntilExpired < $this->errorThreshold) {
            return $result->failed(sprintf('Security.txt expires soon (%d days until)', $daysUntilExpiredRounded));
        }

        if ($daysUntilExpired < $this->warningThreshold) {
            return $result->warning(sprintf('Security.txt expires soon (%d days until)', $daysUntilExpiredRounded));
        }

        return $result->ok();
    }
}
