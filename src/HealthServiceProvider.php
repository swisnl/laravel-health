<?php

namespace Swis\Health;

use Illuminate\Console\Scheduling\Schedule;
use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Checks\QueueCheck;
use Spatie\Health\Checks\Checks\ScheduleCheck;
use Spatie\Health\Commands\DispatchQueueCheckJobsCommand;
use Spatie\Health\Commands\ScheduleCheckHeartbeatCommand;
use Spatie\Health\Facades\Health;
use Spatie\Health\ResultStores\InMemoryHealthResultStore;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class HealthServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package->name('laravel-health')
            ->hasConfigFile('swis-health');
    }

    public function packageRegistered(): void
    {
        config([
            'health.result_stores' => [
                InMemoryHealthResultStore::class,
            ],
            'health.notifications.enabled' => false,
            'health.oh_dear_endpoint.enabled' => true,
        ]);
    }

    public function packageBooted(): void
    {
        $checkFactory = $this->app->make(CheckFactory::class);

        Health::checks(array_filter([
            $checkFactory->makeDebugModeCheck(),
            $checkFactory->makeEnvironmentCheck(),
            $checkFactory->makeOptimizedAppCheck(),
            $checkFactory->makeDatabaseCheck(),
            $checkFactory->makeElasticsearchCheck(),
            $checkFactory->makeCacheCheck(),
            $checkFactory->makeMailCheck(),
            $checkFactory->makeHorizonCheck(),
            $checkFactory->makeQueueCheck(),
            $checkFactory->makeUsedDiskSpaceCheck(),
            $checkFactory->makeScheduleCheck(),
            $checkFactory->makeSecurityTxtCheck(),
            $checkFactory->makeSentryCheck(),
        ]));

        $this->callAfterResolving(Schedule::class, function (Schedule $schedule) {
            $checks = Health::registeredChecks();

            if ($checks->contains(fn (Check $check) => $check instanceof ScheduleCheck)) {
                $schedule->command(ScheduleCheckHeartbeatCommand::class)->everyMinute();
            }
            if ($checks->contains(fn (Check $check) => $check instanceof QueueCheck)) {
                $schedule->command(DispatchQueueCheckJobsCommand::class)->everyMinute();
            }
        });
    }
}
