<?php

namespace Swis\Health;

use Laravel\Horizon\Contracts\MasterSupervisorRepository;
use Spatie\Health\Checks\Checks\CacheCheck;
use Spatie\Health\Checks\Checks\DatabaseCheck;
use Spatie\Health\Checks\Checks\DebugModeCheck;
use Spatie\Health\Checks\Checks\EnvironmentCheck;
use Spatie\Health\Checks\Checks\HorizonCheck;
use Spatie\Health\Checks\Checks\OptimizedAppCheck;
use Spatie\Health\Checks\Checks\QueueCheck;
use Spatie\Health\Checks\Checks\ScheduleCheck;
use Swis\Health\Checks\ElasticsearchCheck;
use Swis\Health\Checks\MailCheck;
use Swis\Health\Checks\SecurityTxtCheck;
use Swis\Health\Checks\SentryCheck;
use Swis\Health\Checks\UsedDiskSpaceCheck;
use Swis\Laravel\Elasticsearch\Client as ElasticsearchClient;
use Symfony\Component\Process\Process;

class CheckFactory
{
    public function makeCacheCheck(): ?CacheCheck
    {
        if (config('swis-health.checks.cache.enabled') === false) {
            return null;
        }

        return CacheCheck::new();
    }

    public function makeDatabaseCheck(): ?DatabaseCheck
    {
        if (config('swis-health.checks.database.enabled') === false) {
            return null;
        }

        $usesDatabase = config('database.connections.mysql.database', 'forge') !== 'forge';

        return config('swis-health.checks.database.enabled') === true || $usesDatabase
            ? DatabaseCheck::new()
            : null;
    }

    public function makeDebugModeCheck(): ?DebugModeCheck
    {
        if (config('swis-health.checks.debug_mode.enabled') === false) {
            return null;
        }

        return DebugModeCheck::new();
    }

    public function makeElasticsearchCheck(): ?ElasticsearchCheck
    {
        if (config('swis-health.checks.elasticsearch.enabled') === false) {
            return null;
        }

        $hasInstalledElasticsearch = class_exists(ElasticsearchClient::class);
        $hasEnabledElasticsearch = config('elasticsearch.enabled');

        return config('swis-health.checks.elasticsearch.enabled') === true || $hasInstalledElasticsearch
            ? ElasticsearchCheck::new()
                ->if($hasEnabledElasticsearch)
            : null;
    }

    public function makeEnvironmentCheck(): ?EnvironmentCheck
    {
        if (config('swis-health.checks.environment.enabled') === false) {
            return null;
        }

        return EnvironmentCheck::new();
    }

    public function makeHorizonCheck(): ?HorizonCheck
    {
        if (config('swis-health.checks.horizon.enabled') === false) {
            return null;
        }

        $hasInstalledHorizon = interface_exists(MasterSupervisorRepository::class);
        $hasEnabledHorizon = config('queue.default') === 'redis';

        return config('swis-health.checks.horizon.enabled') === true || $hasInstalledHorizon
            ? HorizonCheck::new()
                ->if($hasEnabledHorizon)
            : null;
    }

    public function makeMailCheck(): ?MailCheck
    {
        if (config('swis-health.checks.mail.enabled') === false) {
            return null;
        }

        return MailCheck::new();
    }

    public function makeOptimizedAppCheck(): ?OptimizedAppCheck
    {
        if (config('swis-health.checks.optimized_app.enabled') === false) {
            return null;
        }

        return OptimizedAppCheck::new();
    }

    public function makeQueueCheck(): ?QueueCheck
    {
        if (config('swis-health.checks.queue.enabled') === false) {
            return null;
        }

        $usesQueue = config('queue.default', 'sync') !== 'sync';

        return QueueCheck::new()
            ->if($usesQueue);
    }

    public function makeScheduleCheck(): ?ScheduleCheck
    {
        if (config('swis-health.checks.schedule.enabled') === false) {
            return null;
        }

        // Since Laravel 11 we can't check if there are scheduled events from within a package service provider,
        // so we just assume there are, as most projects will have them.
        // If your project doesn't, you should disable this check.
        // FIXME: Figure out a way to make auto-discovery work again.
        $hasScheduledEvents = true;
        $usesQueue = config('queue.default', 'sync') !== 'sync';

        return ScheduleCheck::new()
            ->heartbeatMaxAgeInMinutes(2)
            ->if($hasScheduledEvents || $usesQueue);
    }

    public function makeSecurityTxtCheck(): ?SecurityTxtCheck
    {
        if (config('swis-health.checks.security_txt.enabled') === false) {
            return null;
        }

        return SecurityTxtCheck::new()
            ->file(public_path('.well-known/security.txt'))
            ->if(fn () => file_exists(public_path('.well-known/security.txt')));
    }

    public function makeSentryCheck(): ?SentryCheck
    {
        if (config('swis-health.checks.sentry.enabled') === false) {
            return null;
        }

        $usesSentry = config('logging.default') === 'sentry'
            || (config('logging.default') === 'stack' && in_array('sentry', config('logging.channels.stack.channels'), true));

        return SentryCheck::new()
            ->if($usesSentry);
    }

    public function makeUsedDiskSpaceCheck(): ?UsedDiskSpaceCheck
    {
        if (config('swis-health.checks.used_disk_space.enabled') === false) {
            return null;
        }

        return UsedDiskSpaceCheck::new()
            ->filesystemName(storage_path('app'))
            ->warnWhenUsedSpaceIsAbovePercentage(config('swis-health.checks.used_disk_space.warning_threshold', 85))
            ->failWhenUsedSpaceIsAbovePercentage(config('swis-health.checks.used_disk_space.error_threshold', 95))
            ->if(function () {
                // If the app has files, it has a dedicated mount, otherwise it is mounted to /tmp
                $process = Process::fromShellCommandline(sprintf('df -P | grep -q %s$', base_path('storage/app')));
                $process->run();

                return $process->isSuccessful();
            });
    }
}
