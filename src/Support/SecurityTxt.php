<?php

namespace Swis\Health\Support;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class SecurityTxt
{
    public static function createFromUrl(string $url, int $timeout = 30): self
    {
        return self::createFromString(Http::timeout($timeout)->get($url)->body());
    }

    public static function createFromFile(string $path): self
    {
        return self::createFromString(file_get_contents($path));
    }

    public static function createFromString(string $securityTxt): self
    {
        $values = Str::of($securityTxt)
            ->split('/\r\n|\r|\n/')
            ->map(fn (string $line) => trim($line))
            // Remove empty lines
            ->filter()
            // Remove comments
            ->reject(fn (string $line) => Str::startsWith($line, '#'))
            // Explode to key/value pairs
            ->map(fn (string $line) => array_map('trim', explode(':', $line, 2)))
            // Remove invalid lines
            ->filter(fn (array $tuple) => count($tuple) === 2)
            // Group by directive
            ->groupBy(fn (array $tuple) => strtolower($tuple[0]))
            // Map to values
            ->map->map(fn (array $tuple) => $tuple[1])
            ->toArray();

        return new self($securityTxt, $values);
    }

    public function __construct(protected string $rawContents, protected array $values) {}

    public function getRawContents(): string
    {
        return $this->rawContents;
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function contact(): array
    {
        return $this->values['contact'] ?? [];
    }

    public function expirationDate(): Carbon
    {
        return Carbon::parse($this->values['expires'][0]);
    }

    public function encryption(): array
    {
        return $this->values['encryption'] ?? [];
    }

    public function acknowledgments(): array
    {
        return $this->values['acknowledgments'] ?? [];
    }

    public function preferredLanguages(): array
    {
        return array_key_exists('preferred-languages', $this->values)
            ? array_map('trim', explode(',', $this->values['preferred-languages'][0]))
            : [];
    }

    public function canonical(): array
    {
        return $this->values['canonical'] ?? [];
    }

    public function policy(): array
    {
        return $this->values['policy'] ?? [];
    }

    public function hiring(): array
    {
        return $this->values['hiring'] ?? [];
    }

    public function csaf(): array
    {
        return $this->values['csaf'] ?? [];
    }
}
