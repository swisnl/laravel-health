# Monitor the health of a Laravel application

[![Latest Version on Packagist](https://img.shields.io/packagist/v/swisnl/laravel-health.svg?style=flat-square)](https://packagist.org/packages/swisnl/laravel-health)
[![Total Downloads](https://img.shields.io/packagist/dt/swisnl/laravel-health.svg?style=flat-square)](https://packagist.org/packages/swisnl/laravel-health)

Using this package you can monitor the health of your application by registering checks using [spatie/laravel-health](https://spatie.be/docs/laravel-health/v1). It contains a default set of checks for SWIS applications, but you can also add your own checks.

## Installation

You can install the package via composer:

```bash
composer require swisnl/laravel-health
```

## Usage

This package is plug and play! If you would like to add your own checks, please see [the documentation](https://spatie.be/docs/laravel-health/v1/basic-usage/registering-your-first-check).

## Default checks

* [Debug mode](https://spatie.be/docs/laravel-health/v1/available-checks/debug-mode)
* [Environment](https://spatie.be/docs/laravel-health/v1/available-checks/environment)
* [Optimized app](https://spatie.be/docs/laravel-health/v1/available-checks/cached-config-routes-and-events)
* [Database](https://spatie.be/docs/laravel-health/v1/available-checks/db-connection) (when applicable)
* [Cache](https://spatie.be/docs/laravel-health/v1/available-checks/cache)
* [Horizon](https://spatie.be/docs/laravel-health/v1/available-checks/horizon) (when applicable)
* [Queue](https://spatie.be/docs/laravel-health/v1/available-checks/queue) (when applicable)
* [Used disk space](https://spatie.be/docs/laravel-health/v1/available-checks/used-disk-space) (when applicable)
* [Security.txt](src/Checks/SecurityTxtCheck.php) (when applicable)
* [Schedule](https://spatie.be/docs/laravel-health/v1/available-checks/schedule) (when applicable)

If you want to disable a check, you can publish the config file and set the `enabled` key to `false`.

```bash
php artisan vendor:publish --tag="health-config"
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Security Vulnerabilities

If you discover any security related issues, please email security@swis.nl instead of using the issue tracker.

## Credits

- [Jasper Zonneveld](https://github.com/JaZo)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
